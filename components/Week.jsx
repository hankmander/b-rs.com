import deburr from 'lodash/deburr'
import kebabCase from 'lodash/kebabCase'

const sortObject = (o) =>
  Object.keys(o)
    .sort()
    .reduce((r, k) => ((r[k] = o[k]), r), {})

export default function Week({ week, showDetails, toggleShowDetails }) {
  const d = week.reduce((accumulator, { SellStartDate, AssortmentText }) => {
    accumulator[SellStartDate] = [...new Set([...(accumulator[SellStartDate] || []), AssortmentText])]
    return accumulator
  }, {})

  const dates = sortObject(d)

  return Object.keys(dates) ? (
    Object.keys(dates).map((date) =>
      dates[date].map((AssortmentText) => (
        <div key={date + AssortmentText}>
          <h3 style={{ opacity: 0.6 }}>
            {date.substring(0,10)} -{' '}
            <a
              href={`https://www.systembolaget.se/sok-dryck/?assortmenttext=${AssortmentText.replace(
                /&amp;/g,
                '%26'
              )}&productLaunchFrom=${date.substring(0,10)}&productLaunchTo=${date.substring(0,10)}&sortfield=Name&categoryLevel1=Öl&fullassortment=1`}
              dangerouslySetInnerHTML={{ __html: AssortmentText }}
            ></a>
            <span className='showDetails' onClick={() => toggleShowDetails(!showDetails)}>
              {showDetails ? 'Dölj' : 'Visa'} detaljer
            </span>
          </h3>
          <table>
            <thead>
              <tr>
                <th>Producent</th>
                <th>Namn</th>
                {showDetails && (
                  <>
                    <th>Nr</th>
                    <th>Land</th>
                    <th>Vol.</th>
                    <th>Pris</th>
                    <th>%</th>
                    <th>Stil</th>
                  </>
                )}
                <th style={{ width: '10%' }}>Länk</th>
              </tr>
            </thead>
            <tbody>
              {week.map(
                (article) =>
                  article.SellStartDate === date &&
                  article.AssortmentText === AssortmentText && (
                    <tr key={article.ProductNumber} data-sortiment={article.AssortmentText}>
                      <td>{article.ProducerName}</td>
                      <td dangerouslySetInnerHTML={{ __html: `${article.ProductNameBold} ${article.ProductNameThin || ''}` }}></td>
                      {showDetails && (
                        <>
                          <td>{article.ProductNumber}</td>
                          <td>{article.Country}</td>
                          <td>{article.Volume / 10}cl</td>
                          <td>{article.Price}:-</td>
                          <td>{article.AlcoholPercentage}</td>
                          <td>{article.Style}</td>
                        </>
                      )}
                      <td>
                        <a
                          href={`https://www.systembolaget.se/dryck/ol/${deburr(kebabCase(article.ProductNameBold))}-${
                            article.ProductNumber
                          }`}
                        >
                          Bolaget
                        </a>
                      </td>
                    </tr>
                  )
              )}
            </tbody>
          </table>
          <style jsx>{`
            a,
            .showDetails {
              color: #0b7b3e;
              text-decoration: underline;
            }
            .showDetails {
              display: none;
              line-height: 1.9em;
              float: right;
              z-index: 100;
              cursor: pointer;
              font-size: 16px;
            }
            h3:first-of-type .showDetails {
              display: unset;
            }
          `}</style>
        </div>
      ))
    )
  ) : (
    <p>Vit vecka</p>
  )
}
