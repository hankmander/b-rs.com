import Head from 'next/head'

export default function Home() {

  return (
    <div className='container'>
      <Head>
        <title>Systemvetaren - När släpps bärsen?</title>
        <meta property='og:title' content='Systemvetaren - När släpps bärsen?' />
        <meta name='viewport' content='width=device-width, initial-scale=1' />
        <meta name='description' content='Vilka törstsläckare lansereras på bolaget denna vecka?' />
        <meta property='og:description' content='Vilka törstsläckare lansereras på bolaget denna vecka?' />
        <meta property='og:image' content='/beer-shelf.jpg' />
        <meta property='og:type' content='website' />
        <meta name='twitter:card' content='summary' />
        <meta name='twitter:title' content='Systemvetaren - När släpps bärsen?' />
        <meta name='twitter:description' content='Vilka törstsläckare lansereras på bolaget denna vecka?' />
        <meta name='twitter:creator' content='@hanktard' />
        <meta name='twitter:image' content='/beer-shelf.jpg' />
        <link rel='icon' href='/beer.png' />
        <link rel='stylesheet' href='/light.min.css' />
      </Head>
      <h1 style={{ marginBottom: -10 }}>Systemvetaren</h1>
      <p>Tillfälligt avbrott pga förändringar i Systembolagets API.</p>
      <footer style={{ marginTop: 50, opacity: 0.6 }}>
        Skapat av <a rel="author" href='https://tiburon.se/'>Tiburon - Shopify Solutions</a>
      </footer>
      <script async defer src='https://scripts.simpleanalyticscdn.com/latest.js'></script>
      <noscript>
        <img src='https://queue.simpleanalyticscdn.com/noscript.gif' alt='' />
      </noscript>
    </div>
  )
}
